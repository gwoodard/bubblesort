/*
George Woodard
CS 215
10/16/2014
Programming Assignment #7
Bubble Sort Algorithm

*/


#include <iostream>
using namespace std;
typedef int myType;

//Method used to store each number when swapping in the array
void Swap(myType a[], int &m, int &n)
{
	myType temp;
	temp = a[m];
	a[m] = a[n];
	a[n] = temp;
}


//Bubble Sort algorithm that will sort each number
void bubbleSort(myType a[], int N)
{
	//Declares the array is not sorted so it will enter the for loop and pass thru each number and sort them
	bool sorted = false;
	for(int pass = 1; (pass < N) && !sorted; ++pass)
	{
		sorted = true;
		for(int index = 0; index < N - pass; ++pass)
		{
			//Checks each index in the array to see if sorted and swaps them if they are not already sorted
			int nextIndex = index + 1;
			if(a[index] > a[nextIndex])
			{
				swap(a[index], a[nextIndex]);
				sorted = false;
			}
		}
	}
}


int main()
{
	const int SIZE = 5;
	myType a[SIZE];

	int i;
	for (i = 0; i < SIZE; i++)
	{
		//Prompts the user for input of 5 numbers in the array
		cout<<"Enter a whole number: ";
		cin>>a[i];
	}

	//Calls the Bubble Sort Algorithm
	bubbleSort(a, SIZE);

	//Displays each of the numbers in the sorted order
	for(i = 0; i < SIZE; i++)
		cout<<a[i]<<endl;

	return 0;

}