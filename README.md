# Bubble Sort #

### **About:** ###

Repo containing Bubble Sort program that allows users to input five integers in an array. The numbers are then sorted based on the Bubble Sort Algorithm and displayed in the final output.

### **IDE:**
Visual Studio

### **Input:** ###
5 Integers

### **Input:** ###
All 5 integers sorted based on the Bubble Sort algorithm

### **Language:** ###
C++